/* Logger.cpp -> A simple logging system
 * =====================================
 * Justin Huang (MistaRhee)
 *
 * Tl;dr Logger is a simle logging system. Just start up the class and let it do
 * it's thing. Logger can be run on a separate thread (and is recommended to be)
 * however, you can set the flag for it to be a static logger.
 *
 * Licensing (Re-iterating what logger.hpp said)
 * =========
 *
 * MIT License
 *
 * Copyright (c) 2018 Justin Huang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * ***************************************************************************************************/

#include "logger_impl.h"

#include <iomanip>

namespace {
#if __unix__
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

inline void CreateFolder(const std::string& dirName) {
  struct stat st = {0};
  if (stat(dirName.c_str(), &st) == -1) {
    mkdir(dirName.c_str(), 0775);
  }
}
#endif

#ifdef _WIN32
#include <Windows.h>
#include "dirent/dirent.h"

inline void CreateFolder(const std::string& dir_name) {
  CreateDirectory(dir_name.c_str(), NULL);
}
#endif

inline bool DirectoryExists(const std::string& dir_name) {
  DIR* dir = NULL;
  dir = opendir(dir_name.c_str());
  if (dir == NULL)
    return false;
  else
    return true;
}

}  // namespace

namespace Logger {

inline std::string CurrentDateTime() {
  std::time_t time = std::time(nullptr);
  std::tm tm = *std::localtime(&time);
  std::stringstream rVal;
  rVal << std::put_time(&tm, "%c %Z");
  return rVal.str();
}

Logger::Logger(std::string file_location, int log_level)
    : log_location(file_location), log_level(log_level) {
#ifndef __LOGGER_NOTHREAD__
  dead = 0;
#endif
}

Logger::~Logger() {}

void Logger::LogImpl(const std::string& contents) {
  std::string stripped_contents = contents;
  while (stripped_contents[stripped_contents.size() - 1] == '\n')
    stripped_contents.erase(stripped_contents.end() - 1);  // Remove trailing new-lines

#ifdef __LOGGER_NOTHREAD__
  std::flog << CurrentDateTime() << " " << stripped_contents << std::endl;
#else
  std::stringstream output;
  output << CurrentDateTime() << " " << stripped_contents;
  std::unique_lock<std::mutex> ul(lock);
  q.Put(output.str());
  cv.notify_all();
#endif
}

#ifndef __LOGGER_NOTHREAD__
void Logger::Start() {
  OpenFileAndEnsureExistence();
  run_thread = std::thread(&Logger::Run, this);
}

void Logger::SetLogLevel(int l) {
  log_level = l;
}
void Logger::SetLogLocation(std::string l) {
  log_location = l;
}

void Logger::Run() {
  done.lock();
  while (!dead) {
    std::unique_lock<std::mutex> ul(lock);
    if (q.Empty())
      cv.wait(ul);
    while (!q.Empty()) {
      std::string out = q.Get();
      flog << out << std::endl;
    }
  }

  done.unlock();
}

void Logger::Kill() {
  dead = 1;
  cv.notify_all();
}

void Logger::BlockUntilDone() {
  done.lock();
  done.unlock();
  run_thread.join();
}

void Logger::OpenFileAndEnsureExistence() {
  // Check if there is a folder in the location -> ensure that the folder exists
  std::string tempStr;
  count = 0;
  for (size_t i = 0; i < log_location.size(); i++) {
    if (log_location[i] == '/') {
      /* There is folder */
      if (!DirectoryExists(tempStr)) {
        if (!tempStr.empty())
          CreateFolder(tempStr);
      }
    } else {
      tempStr += log_location[i];
    }
  }
  tempStr.clear();

  flog.open(log_location, std::ofstream::out | std::ofstream::app);
}

}  // namespace __logger
#endif
