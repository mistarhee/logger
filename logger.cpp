#include "logger.h"

#include <memory>
#include <string>

Logger::Logger __logger__;

void InitLogging(std::string file_location, int log_level) {
  __logger__.SetLogLocation(file_location);
  __logger__.SetLogLevel(log_level);
  __logger__.Start();
}

void StopLogging(bool blocking) {
  __logger__.Kill();
  if (blocking)
    __logger__.BlockUntilDone();
}