# cLogger

A simple logging system

## Dependencies

This project requires the attached q.hpp file from the [queue](https://bitbucket.org/mistarhee/queue) for queues now. Should compile under C++17 and above.

## Usage

Basic usage example:

```
#include "../logger.hpp"

#include <iostream>

int main()
{
    __logger::cLogger mLog("bananas.log", __logger::INFO);
    mLog.start().detach();
    mLog.log(__logger::INFO, "I ate a banana");
    mLog.log(__logger::ERR, "I ate more than just one banana");
    mLog.log(__logger::CRIT, "I ate too many bananas");
    
    mLog.kill();
    mLog.done.lock(); //Just so other death/gc can happen while we're waiting for the backlog to clear up. This just tells the program "Yes, I've actually finished all my backlog"
    mLog.done.unlock();
}
```

Note, the advanced example requires C++14 because I used chrono literals out of laziness. TBH, doesn't really need it.

## License

```
MIT License

Copyright (c) 2018 Justin Huang

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```
