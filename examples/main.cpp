#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#include "../logger.h"
#include "other_file.h"

int main() {
  InitLogging("counting.log", INFO);

  std::thread t1(&LogExample, "banana a");
  std::thread t2(&LogExample, "banana b");
  std::thread t3(&LogExample, "banana c");
  std::thread t4(&LogExample, "banana d");
  std::thread t5(&LogExample, "banana e");

  t1.join();
  t2.join();
  t3.join();
  t4.join();
  t5.join();

  StopLogging(true);

  return 0;
}
