#include "other_file.h"

#include "../logger.h"

void LogExample(std::string identifier) {
  for (int i = 0; i < 10; i++) {
    LOG(INFO, identifier, " counts! ", identifier, " is up to: ", i);
  }
}
