/* Logger.hpp -> A simple logging system
 * =====================================
 * Justin Huang (MistaRhee)
 *
 * Tl;dr Logger is a simle logging system. Just start up the class and let it do
 * it's thing. Logger can be run on a separate thread (and is recommended to be)
 * however, you can set the flag for it to be a static logger.
 *
 * Licensing (Really? You power hungry people -_-)
 * =========
 *
 * MIT License
 *
 * Copyright (c) 2018 Justin Huang
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * ***************************************************************************************************/

#ifndef LOGGER_LOGGER_IMPL_H_
#define LOGGER_LOGGER_IMPL_H_

// Uncomment the following line for no threading (why?)
//#define __LOGGER_NOTHREAD__
#ifndef __LOGGER_NOTHREAD__
#include <condition_variable>
#include <mutex>
#include <thread>

#endif

#include <cstdarg>
#include <cstdio>  //Yes, I'm an old boy. Deal with it
#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

#include "q.hpp"

#define ERR 0
#define WARN 1
#define INFO 2
#define DEBUG 3

namespace {

/* Unpack functions. Yey */
void unpack(std::ostringstream& ss) {
  return;
}
template <class T, class... TArgs>
void unpack(std::ostringstream& ss, T value, TArgs... FArgs) {
  ss << " " << value;
  unpack(ss, FArgs...);
}
}  // namespace

namespace Logger {

class Logger {
 public:
  Logger() : Logger("wooden.log") {}
  Logger(std::string fout) : Logger(fout, ERR) {}  // Logs to the specified file
  Logger(std::string fout, int);
  Logger(const Logger&) = delete;
  ~Logger();

  // Concactenates all the arguments with a space between
  template <class... TArgs>
  void Log(int log_level, TArgs... FArgs) {
    if (log_level > this->log_level) {
      return;
    }
    std::ostringstream out;
    unpack(out, FArgs...);
    LogImpl(out.str());
  }

  void SetLogLocation(std::string filepath);
  void SetLogLevel(int log_level);
#ifndef __LOGGER_NOTHREAD__
  void Start();
  void Run();
  void Kill();

  void BlockUntilDone();
#endif

 private:
  void LogImpl(const std::string& contents);
  void OpenFileAndEnsureExistence();

#ifndef __LOGGER_NOTHREAD__
  Queue<std::string> q;
  bool dead;
  std::mutex lock;

  // A blocking mutex to ensure that all things have been logged
  // before deleting the object (usually used if the main function has
  // a deconstructor or something(
  std::mutex done;
  std::condition_variable cv;
#endif
  std::string log_location;
  std::fstream flog;
  unsigned int count;
  int log_level;
  std::thread run_thread;
};
}  // namespace Logger

#endif  // LOGGER_LOGGER_IMPL_H_
