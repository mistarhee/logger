#ifndef LOGGER_LOGGER_H_
#define LOGGER_LOGGER_H_

#include "logger_impl.h"

/*
ERR 0
WARN 1
INFO 2
DEBUG 3
*/

#include <memory>
#include <string>

extern Logger::Logger __logger__;

void InitLogging(std::string file_location, int log_level);

void StopLogging(bool blocking);

#define LOG(x, ...) __logger__.Log(x, __VA_ARGS__)

#endif  // LOGGER_LOGGER_H_